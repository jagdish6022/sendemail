
const AWS = require('aws-sdk');

AWS.config.update({
  accessKeyId: '',
  secretAccessKey: '',
  region: 'us-east-2'
});

const emailClient = new AWS.SES({ apiVersion: '2010-12-01' });

// eslint-disable-next-line no-unused-vars
exports.sendEmail = async () => {
  try {
    console.log('start');
    const params = {
      Source: 'support@cureitt.com', // Replace with your verified email address
      Destination: {
        ToAddresses: ['jagdish@woyce.io'] /// Destination Email Address
      },
      Message: {
        Subject: {
          Charset: 'UTF-8',
          Data: 'Hello test Email',
        },
        Body: {
          Html: {
            Charset: 'UTF-8',
            Data: 'Test Email Body Text',
          },
        },
      },
      // ReplyToAddresses: [replyTo],
    };
    await emailClient.sendEmail(params).promise();
    return { status: 'success', message: 'sent Email' };
  } catch (error) {
    return { status: 'success', message: error.message };
  }
};