const express = require("express");
const bodyParser = require("body-parser");
const AWS = require("aws-sdk");
const cors = require("cors");
const fs = require("fs");
const { sendEmail } = require("./send-email");
const app = express();

const client = new AWS.SecretsManager({
  region: "us-east-2",
});


app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.json({ limit: "100mb", extended: false }));
app.use(bodyParser.urlencoded({ limit: "100mb", extended: false }));


app.post('/sendEmail', async (req, res) => {
    const ress = await sendEmail();
    return res.status(200).json(ress);
});
const port = 3000;

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});

